package object_classes_interfaces;

public interface Fooable {
	void foo();
	void foo1();
	
	default void foo3() {
		System.out.println("I'm deafault method");
	}
}

package object_classes_interfaces;

public class TestAppl {
	public static void main(String[] args) {
		MyClass myObject = new MyClass();
		
		myObject.foo();
		
		Fooable myFoo = new MyClass();
		
		myFoo.foo();
		
		
		Fooable myInterface = new Fooable() {
			
			@Override
			public void foo() {
				System.out.println("Hello world!");
			}

			@Override
			public void foo1() {
				System.out.println("Hello2");
			}
		};
		
		myInterface.foo();
		myInterface.foo1();
		
		Fooable2 foo2 = new MyClass();
		Fooable foo = new MyClass();
		
		foo.foo3();
		
		
		Fooable myFooable45 = new Fooable() {
			
			@Override
			public void foo1() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void foo() {
				// TODO Auto-generated method stub
				
			}
		};
	}
}

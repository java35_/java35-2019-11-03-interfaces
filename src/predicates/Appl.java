package predicates;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.function.Predicate;

public class Appl {
	public static void main(String[] args) {
//		MyArrays myArrays = new MyArrays();

		// 1
//		myArrays.printEvenNumbers(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9});
		
		// 2 Сокращённый
//		int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9};
//		myArrays.printEvenNumbers(array);
		
		// 3 Полный
		Integer[] array = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 3};
//		MyArrays.printEvenNumbers(array);
//		MyArrays.printOddNumbers(array);
//		MyArrays.printNumbersLessThenFive(array);
		
//		TestInterface test = (value) -> value % 2 == 0;
//		
//		MyArrays.printNumbers(array, test);
//		MyArrays.printNumbers(array, (value) -> value % 2 == 0);
		
		Predicate<Integer> predicate = (value) -> value % 2 == 0;
		
		MyArrays.printNumbers(array, predicate);
		MyArrays.printNumbers(array, (value) -> value % 2 != 0);
		MyArrays.printNumbers(new String[] {"Hello", "Hi", "Privet"}, 
					(value) -> value.equals("Hello") || value.equals("Hi") );
	}
}

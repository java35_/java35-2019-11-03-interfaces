package predicates;

import java.util.function.Predicate;

public class MyArrays {
//	public static void printEvenNumbers(int[] array) {
//		
//		TestInterface test = (value) -> {
//			return value % 2 == 0;
//		};
//		printNumbers(array, test);
//	}
//	
//	public static void printOddNumbers(int[] array) {
//		TestInterface test = (value) -> value % 2 != 0;
//		printNumbers(array, test);
//	}
//	
//	public static void printNumbersLessThenFive(int[] array) {
//		TestInterface test = new TestInterface() {
//			
//			@Override
//			public boolean test(int value) {
//				return value < 5;
//			}
//		};
//		printNumbers(array, test);
//	}
//	
//	private static void printNumbers(int[] array, TestInterface test) {
//		for (int value : array) {
//			if (test.test(value))
//				System.out.print(value + " ");
//		}
//		System.out.println();
//	}
//	
//	interface TestInterface {
//		public boolean test(int value);
//	}

//	public static void printNumbers(int[] array, TestInterface test) {
//		for (int value : array) {
//			if (test.test(value))
//				System.out.print(value + " ");
//		}
//		System.out.println();
//	}
	
	public static <T> void printNumbers(T[] array, Predicate<T> test) {
		for (T value : array) {
			if (test.test(value))
				System.out.print(value + " ");
		}
		System.out.println();
	}
}
